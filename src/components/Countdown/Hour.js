import React, { useState } from "react";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import css from "./Hour.module.css";

function Hour() {
	const [hour, setHour] = useState(0);
	const [minute, setMinute] = useState(0);
	const [second, setSecond] = useState(0);

	const addHour = () => {
		setHour((prevValue) => prevValue + 1);
	};
	const subtractHour = () => {
		setHour((prevValue) => prevValue - 1);
	};
	const addMinute = () => {
		setMinute((prevValue) => prevValue + 1);
	};
	const subtractMinute = () => {
		setMinute((prevValue) => prevValue - 1);
	};
	const addSecond = () => {
		setSecond((prevValue) => prevValue + 1);
	};
	const subtractSecond = () => {
		setSecond((prevValue) => prevValue - 1);
	};
	return (
		<div>
			<div className={css.buttons}>
				<div className={css.buttons_items}>
					<ExpandLessIcon className={css.addSubbuttons} onClick={addHour} />
					<div className={css.time}>{hour}</div>
					<ExpandMoreIcon onClick={subtractHour} />
				</div>
				<div className={css.buttons_items}>
					<ExpandLessIcon onClick={addMinute} />
					<div className={css.time}>{minute}</div>
					<ExpandMoreIcon onClick={subtractMinute} />
				</div>
				<div className={css.buttons_items}>
					<ExpandLessIcon onClick={addSecond} />
					<div className={css.time}>{second}</div>
					<ExpandMoreIcon onClick={subtractSecond} />
				</div>
			</div>
			<div className={css.startBtn}>
				<button>Start</button>
			</div>
		</div>
	);
}

export default Hour;
