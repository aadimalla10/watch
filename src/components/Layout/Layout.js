import React from "react";
import css from "./Layout.module.css";

function Layout(props) {
	return (
		<div className={css.display}>
			<div className={css.display_item}>{props.children}</div>
		</div>
	);
}

export default Layout;
