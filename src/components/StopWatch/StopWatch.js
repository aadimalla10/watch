import React, { useState, useEffect } from "react";
import css from "./StopWatch.module.css";

function StopWatch() {
	const [time, setTime] = useState(0);
	const [timerOn, setTimerOn] = useState(false);

	useEffect(() => {
		let interval = null;

		if (timerOn) {
			interval = setInterval(() => {
				setTime((prevTime) => prevTime + 10);
			}, 10);
		} else if (!timerOn) {
			clearInterval(interval);
		}

		return () => clearInterval(interval);
	}, [timerOn]);

	const startHandler = () => {
		setTimerOn(true);
	};
	const stopHandler = () => {
		setTimerOn(false);
	};
	const resumeHandler = () => {
		setTimerOn(true);
	};
	const resetHandler = () => {
		setTimerOn(false);
		setTime(0);
	};

	return (
		<div>
			<div className={css.display_item}>
				<span>{("0" + Math.floor((time / 60000) % 60)).slice(-2)}:</span>
				<span>{("0" + Math.floor((time / 1000) % 60)).slice(-2)}:</span>
				<span>{("0" + ((time / 10) % 100)).slice(-2)}</span>
			</div>
			<div className={css.Buttons}>
				{!timerOn && time === 0 && (
					<button className={css.Buttons_item} onClick={startHandler}>
						Start
					</button>
				)}
				{timerOn && (
					<button className={css.Buttons_item} onClick={stopHandler}>
						Stop
					</button>
				)}
				{!timerOn && time > 0 && (
					<button className={css.Buttons_item} onClick={resumeHandler}>
						Resume
					</button>
				)}
				{!timerOn && time > 0 && (
					<button className={css.Buttons_item} onClick={resetHandler}>
						Reset
					</button>
				)}
			</div>
		</div>
	);
}

export default StopWatch;
