import React, { useState } from "react";

import css from "./Time.module.css";

function Time() {
	let time = new Date().toLocaleTimeString();
	const [currentTime, setCurrentTime] = useState(time);

	const UpdateTime = () => {
		time = new Date().toLocaleTimeString();
		setCurrentTime(time);
	};
	setInterval(UpdateTime, 1000);
	return <div className={css.display_item}>{currentTime}</div>;
}

export default Time;
