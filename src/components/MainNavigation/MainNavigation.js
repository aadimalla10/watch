import React from "react";
import { Link } from "react-router-dom";
import css from "./MainNavigation.module.css";
import AccessTimeIcon from "@material-ui/icons/AccessTime";
import TimerIcon from "@material-ui/icons/Timer";
import AlarmIcon from "@material-ui/icons/Alarm";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";

function MainNavigation() {
	return (
		<div>
			<nav>
				<ul>
					<li>
						<Link className={css.listItem} to="/">
							<AccessTimeIcon className={css.icon} />
						</Link>
					</li>
					<li>
						<Link className={css.listItem} to="/stopwatch">
							<TimerIcon className={css.icon} />
						</Link>
					</li>
					<li>
						<Link className={css.listItem} to="/alarm">
							<AlarmIcon className={css.icon} />
						</Link>
					</li>
					<li>
						<Link className={css.listItem} to="/countdown">
							<HourglassEmptyIcon className={css.icon} />
						</Link>
					</li>
				</ul>
			</nav>
		</div>
	);
}

export default MainNavigation;
