import { BrowserRouter, Route, Switch } from "react-router-dom";

import Time from "./components/Time/Time";
import StopWatch from "./components/StopWatch/StopWatch";
import MainNavigation from "./components/MainNavigation/MainNavigation";
import Layout from "./components/Layout/Layout";
import Countdown from "./components/Countdown/Countdown";
import AlarmClock from "./components/Alarm/AlarmClock";

function App() {
	return (
		<div className="App">
			<>
				<BrowserRouter>
					<MainNavigation />
					<Switch>
						<Route path="/" exact={true}>
							<Layout>
								<Time />
							</Layout>
						</Route>
						<Route path="/stopwatch">
							<Layout>
								<StopWatch />
							</Layout>
						</Route>
						<Route path="/alarm">
							<Layout>
								<AlarmClock />
							</Layout>
						</Route>
						<Route path="/countdown">
							<Layout>
								<Countdown />
							</Layout>
						</Route>
					</Switch>
				</BrowserRouter>
			</>
		</div>
	);
}

export default App;
